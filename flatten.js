let flattedArray = [];
function flatten(elements) {
  for (let index = 0; index < elements.length; index++) {
    if (Array.isArray(elements[index])) {
      flatten(elements[index]);
    } else {
      flattedArray.push(elements[index]);
    }
  }
  return flattedArray;
}

module.exports = flatten;
