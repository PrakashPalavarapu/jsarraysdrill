function reduce(items, callBack = undefined,startingValue=0) {
  if(callBack){
    if (Array.isArray(items)) {
      for (let index = 0; index < items.length; index++) {
        startingValue = callBack(startingValue, items[index]);
      }
      return startingValue;
    }
    return `TypeError : reduce is not a function`;
  }return `TypeError : undefined is not a function`;
}
module.exports = reduce;