function each(items, callBack = undefined) {
  if (callBack) {
    if (Array.isArray(items)) {
      for (let index = 0; index < items.length; index++) {
        console.log(callBack(items, index));
      }
    }
    console.log(`TypeError : each is not a function`);
    return;
  }
  console.log(`TypeError : undefined is not a function`);
}
module.exports = each;
