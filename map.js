function map(items, callBack=undefined) {
  let mapResult = [];
  if(callBack){
    if (Array.isArray(items)) {
      for (let index = 0; index < items.length; index++) {
        mapResult.push(callBack(items[index]));
      }
      return mapResult;
    }
    return `TypeError : map is not a function`;
  }return `TypeError : undefined is not a function`;
}
module.exports = map;
