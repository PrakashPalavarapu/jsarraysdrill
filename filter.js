function filter(items, callBack = undefined) {
  let filterResult = [];
  if (callBack) {
    if(Array.isArray(items)){
      for (let index = 0; index < items.length; index++) {
        if (callBack(items[index])) {
          filterResult.push(items[index]);
        }
      }
      return filterResult;
    }return `TypeError : filter is not a function`;
  }return `TypeError : undefined is not a function`;
}

module.exports = filter;
