function find(items, callBack = undefined) {
  if(callBack){
    if (Array.isArray(items)) {
      for (let index = 0; index < items.length; index++) {
        if (callBack(items[index])) {
          return items[index];
        }
      }
      return undefined;
    }
    return `TypeError : find is not a function`;
  }return `TypeError : undefined is not a function`;
}
module.exports = find;
